import { Color } from 'three';

export const colors = {
  magenta: '#ea4e82',
  yellow: '#fec700',
  cyan: '#6ac7e6',
  blue: '#3563ad'
};

export function getRandomColor(currentColor, addWhite) {
  const keyColors = Object.keys(colors);
  let randomKey;
  let color;

  if (addWhite) keyColors.push('white');

  if (!currentColor) {
    randomKey = Math.floor(Math.random() * keyColors.length);

    if (randomKey == 'white') color = new Color('white');
    else color = new Color(colors[keyColors[randomKey]]);
  } else {
    do {
      randomKey = Math.floor(Math.random() * keyColors.length);

      if (randomKey == 'white') color = new Color('white');
      else color = new Color(colors[keyColors[randomKey]]);
    } while (
      color.r == currentColor.r &&
      color.g == currentColor.g &&
      color.b == currentColor.b
    );
  }

  return color;
}
