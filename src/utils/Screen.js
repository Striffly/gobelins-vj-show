export function getViewport() {
  const distance = 500;
  const aspect = window.innerWidth / window.innerHeight;
  const height = distance;
  const width = height * aspect;

  return { width, height };
}
