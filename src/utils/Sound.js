import sono from 'sono';
import 'sono/effects';

class Sound {
  constructor() {
    this.init('static/sounds/music.mp3');
  }

  init(filePath) {
    if (this.sound)
      this.sound.destroy()

    this.sound = sono
      .create({
        url: [filePath],
        loop: true
      })
      .play();

    this.analyse = this.sound.effects.add(
      sono.analyser({
        fftSize: 256,
        smoothing: 0.7
      })
    );
  }

  getFrequencies() {
    return this.analyse.getFrequencies();
  }

  averageAmplitude(wave) {
    let sum = 0;
    for (let i = 0; i < wave.length; i++) {
      sum += wave[i];
    }

    return sum / wave.length / 256;
  }

  getAverageAmplitude() {
    return this.averageAmplitude(this.analyse.getWaveform());
  }
}

export default new Sound();
