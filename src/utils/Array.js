export const average = arr => arr.reduce((p, c) => p + c, 0) / arr.length;
