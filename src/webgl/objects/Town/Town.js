import {
  Object3D,
  BoxGeometry,
  MeshLambertMaterial,
  Mesh,
  MeshBasicMaterial
} from 'three';
import { average } from '../../../utils/Array';

class Town extends Object3D {
  constructor() {
    super();

    this.name = 'town';

    const geometry = new BoxGeometry(200, 200, 200);
    const material = new MeshBasicMaterial({
      color: 0xffffff,
      overdraw: 0.5
    });

    const mesh = new Mesh(geometry, material);

    this.add(mesh);
  }

  update(dt = 0, time = 0, frequencies) {
    const value = average(frequencies);

    let min = 100;
    let max = 120;
    const threshold = 0.8;

    if (value < min) {
      min = value;
    }

    if (value > max) {
      max = value;
    }

    const range = max - min || 1;
    const norm = (value - min) / range;

    if (norm > threshold) {
      // got a peak so animate, speed up, glow etc
      if (Math.random() > 0.5) {
        this.position.x = 150;
      } else {
        this.position.x = -150;
      }
    }
  }
}

export default Town;
