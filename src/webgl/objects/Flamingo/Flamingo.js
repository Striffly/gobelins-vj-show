import {
  Object3D,
  CircleGeometry,
  ShaderMaterial,
  PlaneBufferGeometry,
  Mesh,
  Color,
  Texture,
  MeshBasicMaterial,
  TextureLoader,
  Group,
  CylinderBufferGeometry
} from 'three';
import { average } from '../../../utils/Array';

class Flamingo extends Object3D {
  constructor() {
    super();

    this.name = 'flamingo';

    const geometry = new PlaneBufferGeometry(180, 180, 0);
    const texture = new TextureLoader().load('static/textures/flamingo.png');

    this.material = new MeshBasicMaterial({
      map: texture,
      transparent: true,
      side: 2
    });

    const mesh = new Mesh(geometry, this.material);

    const leftWhiteEyeGeometry = new CylinderBufferGeometry(6, 6, 10, 32);
    const leftWhiteEyeMaterial = new MeshBasicMaterial({ color: 0xffffff });
    const leftWhiteEye = new Mesh(leftWhiteEyeGeometry, leftWhiteEyeMaterial);
    rotateObject(leftWhiteEye, 90, 0, 0);

    const leftPupilsGeometry = new CylinderBufferGeometry(3, 3, 12, 32);
    const leftPupilsMaterial = new MeshBasicMaterial({ color: 0x000000 });
    const leftPupils = new Mesh(leftPupilsGeometry, leftPupilsMaterial);
    rotateObject(leftPupils, 90, 0, 0);

    this.leftEye = new Group();
    this.leftEye.add(leftWhiteEye);
    this.leftEye.add(leftPupils);
    this.leftEye.position.set(23, 84, 2);

    const rightWhiteEyeGeometry = new CylinderBufferGeometry(6, 6, 10, 32);
    const rightWhiteEyeMaterial = new MeshBasicMaterial({ color: 0xffffff });
    const rightWhiteEye = new Mesh(
      rightWhiteEyeGeometry,
      rightWhiteEyeMaterial
    );
    rotateObject(rightWhiteEye, 90, 0, 0);

    const rightPupilsGeometry = new CylinderBufferGeometry(3, 3, 12, 32);
    const rightPupilsMaterial = new MeshBasicMaterial({ color: 0x000000 });
    const rightPupils = new Mesh(rightPupilsGeometry, rightPupilsMaterial);
    rotateObject(rightPupils, 90, 0, 0);

    this.rightEye = new Group();
    this.rightEye.add(rightWhiteEye);
    this.rightEye.add(rightPupils);
    this.rightEye.position.set(35, 90, 2);

    const flamingo = new Group();
    flamingo.add(mesh);
    flamingo.add(this.leftEye);
    flamingo.add(this.rightEye);
    flamingo.scale.set(0.5, 0.5, 0.5);

    this.add(flamingo);
  }

  update(dt = 0, time = 0, frequencies) {
    const start = 0;
    const range = 40;

    let trimedFrequencies = [];

    for (let i = start; i < start + range; i++) {
      trimedFrequencies.push(frequencies[i]);
    }

    trimedFrequencies = average(trimedFrequencies);

    const scaleAmplitude = trimedFrequencies * 0.8 + 2;
    let scale = dt * scaleAmplitude;

    if (scale > 3.8) {
      scale = 3.8;
    }

    this.leftEye.scale.set(scale, scale, scale);
    this.rightEye.scale.set(scale, scale, scale);
  }
}

function rotateObject(object, degreeX = 0, degreeY = 0, degreeZ = 0) {
  degreeX = (degreeX * Math.PI) / 180;
  degreeY = (degreeY * Math.PI) / 180;
  degreeZ = (degreeZ * Math.PI) / 180;

  object.rotateX(degreeX);
  object.rotateY(degreeY);
  object.rotateZ(degreeZ);
}

export default Flamingo;
