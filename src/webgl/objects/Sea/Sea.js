import {
  Group,
  Mesh,
  MeshBasicMaterial,
  Object3D,
  PlaneBufferGeometry
} from 'three';
import { Flamingo } from '../Flamingo';
import { getViewport } from '../../../utils/Screen';

import { average } from '../../../utils/Array';

class Sea extends Object3D {
  constructor() {
    super();

    this.name = 'sea';

    const { width, height } = getViewport();

    this.oldTime = 0;

    const backgroundGeometry = new PlaneBufferGeometry(width, height, 0);
    const backgroundMaterial = new MeshBasicMaterial({ color: 0x6ac7e6 });
    const background = new Mesh(backgroundGeometry, backgroundMaterial);
    this.add(background);

    this.flamingos = [];

    let flamingos = new Group();

    for (let i = 0; i < 4; i++) {
      let subFlamingos = [];

      for (let j = 0; j < width / 100; j++) {
        const flamingo = new Flamingo();
        flamingo.position.set(j * 100, i * 145, 2);

        let rotation = 25; //Math.floor(Math.random()*25) + 1; // this will get a number between 1 and 99;
        if (j % 2 === 0) {
          rotation = rotation * -1;
        }
        rotateObject(flamingo, 0, 0, rotation);

        if (i % 2 === 0) {
          rotateObject(flamingo, 0, 180, 0);
        }

        subFlamingos.push(flamingo);
        flamingos.add(flamingo);
      }

      this.flamingos.push(subFlamingos);
    }

    flamingos.position.set(-500, -250, 2);
    this.add(flamingos);

    this.bigFlamingos = [];
    const flamingo = new Flamingo();
    flamingo.position.set(-400, -150, 25);
    flamingo.scale.set(3.6, 3.6, 3.6);
    rotateObject(flamingo, 0, 0, -20);
    this.add(flamingo);
    this.bigFlamingos.push(flamingo);

    const flamingo2 = new Flamingo();
    flamingo2.position.set(-100, -150, 25);
    flamingo2.scale.set(3.6, 3.6, 3.6);
    rotateObject(flamingo2, 0, 0, 10);
    this.add(flamingo2);
    this.bigFlamingos.push(flamingo2);

    const flamingo3 = new Flamingo();
    flamingo3.position.set(150, -150, 25);
    flamingo3.scale.set(3.6, 3.6, 3.6);
    rotateObject(flamingo3, 0, 0, 30);
    this.add(flamingo3);
    this.bigFlamingos.push(flamingo3);

    const flamingo4 = new Flamingo();
    flamingo4.position.set(300, -150, 25);
    flamingo4.scale.set(3.6, 3.6, 3.6);
    rotateObject(flamingo4, 0, 0, -15);
    this.add(flamingo4);
    this.bigFlamingos.push(flamingo4);
  }

  update(dt = 0, time = 0, frequencies) {
    const { width, height } = getViewport();
    const value = average(frequencies);

    let min = 80;
    let max = 127;
    const threshold = 0.4;

    if (value < min) {
      min = value;
    }

    if (value > max) {
      max = value;
    }

    const range = max - min || 1;
    const norm = (value - min) / range;

    let currentTime = Math.floor(time);
    if (currentTime > this.oldTime) {
      for (let i = 0; i < this.flamingos.length; i++) {
        let degree = 1;
        if (i % 2 === 0) {
          degree = -1;
        }

        this.flamingos[i].forEach(function(entry) {
          if (Math.degrees(entry.rotation.z) > 0) {
            rotateObject(entry, 0, 0, -50 * degree);
          } else {
            rotateObject(entry, 0, 0, 50 * degree);
          }
        });
      }
    }
    this.oldTime = currentTime;

    this.flamingos[0].forEach(function(entry) {
      let posX = entry.position.x;
      if (posX < -37.5) {
        posX = width + 37.5;
      } else {
        posX -= 1;
      }
      entry.position.set(posX, entry.position.y, entry.position.z);
    });

    this.flamingos[1].forEach(function(entry) {
      let posX = entry.position.x;
      if (posX > width + 37.5) {
        posX = -37.5;
      } else {
        posX += 1;
      }
      entry.position.set(posX, entry.position.y, entry.position.z);
    });

    this.flamingos[2].forEach(function(entry) {
      let posX = entry.position.x;
      if (posX < -37.5) {
        posX = width + 37.5;
      } else {
        posX -= 1;
      }
      entry.position.set(posX, entry.position.y, entry.position.z);
    });

    this.flamingos[3].forEach(function(entry) {
      let posX = entry.position.x;
      if (posX > width + 37.5) {
        posX = -37.5;
      } else {
        posX += 1;
      }
      entry.position.set(posX, entry.position.y, entry.position.z);
    });

    if (norm > threshold) {
      this.bigFlamingos.forEach(function(entry) {
        let posX = entry.position.x;
        if (posX > width / 2) {
          posX = (width / 2) * -1;
        } else {
          posX += 20;
        }

        entry.position.set(posX, entry.position.y, entry.position.z);
      });
    }
  }
}

function rotateObject(object, degreeX = 0, degreeY = 0, degreeZ = 0) {
  object.rotateX(Math.radians(degreeX));
  object.rotateY(Math.radians(degreeY));
  object.rotateZ(Math.radians(degreeZ));
}

// Converts from degrees to radians.
Math.radians = function(degrees) {
  return (degrees * Math.PI) / 180;
};

// Converts from radians to degrees.
Math.degrees = function(radians) {
  return (radians * 180) / Math.PI;
};

export default Sea;
