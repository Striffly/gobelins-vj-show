import {
  Group,
  Color,
  Vector3,
  Mesh,
  TorusBufferGeometry
} from 'three'
import { colors, getRandomColor } from '../../../utils/Colors'
import { getViewport } from '../../../utils/Screen'
import { TweenMax } from 'gsap'

export default class Donuts extends Group {
  constructor() {
    super()

    const ShaderFrogRuntime = require('shaderfrog-runtime')
    this.runtime = new ShaderFrogRuntime()
    this.loaded = false

    this.viewport = getViewport()

    this.columns = 8
    this.lines = 5
    this.smallScale = 0.05
    this.bumpedScaleTHICCDonut = 1.3
    this.thresholdRotationNormal = 30
    this.thresholdRotationFullspeed = 120
    this.thresholdRotation = this.thresholdRotationNormal
    this.wait = false
    this.lowFrequencyCounter = 0
  }

  // Loads the shader with ShaderFrog runtime
  load(light) {
    return new Promise(resolve => {
      this.runtime.load('static/shaders/toon-shader.json', shaderData => {
        this.loaded = true
        this.createMaterials(shaderData, light)
        this.createTHICCDonut()
        this.createSmallDonuts(this.lines, this.columns)

        resolve()
      })
    })
  }

  // Creates materials from the loaded shader and the light position (shader's uniform)
  createMaterials(shaderData, light) {
    this.bigShaderMaterial = this.runtime.get(shaderData.name)
    this.bigShaderMaterial.uniforms.cameraPosition.value = new Vector3(0, 0, 400)
    this.bigShaderMaterial.uniforms.color.value = new Color(colors.yellow)
    this.bigShaderMaterial.uniforms.lightPosition.value = light.position.clone()
    this.bigShaderMaterial.uniforms.edgecolor.value = new Color(colors.yellow)

    this.smallShaderMaterialBlue = this.bigShaderMaterial.clone()
    this.smallShaderMaterialBlue.uniforms.color.value = new Color(colors.blue)
    this.smallShaderMaterialBlue.uniforms.edgecolor.value = new Color(colors.blue)

    this.smallShaderMaterialMagenta = this.bigShaderMaterial.clone()
    this.smallShaderMaterialMagenta.uniforms.color.value = new Color(colors.magenta)
    this.smallShaderMaterialMagenta.uniforms.edgecolor.value = new Color(colors.magenta)

    this.smallShaderMaterialCyan = this.bigShaderMaterial.clone()
    this.smallShaderMaterialCyan.uniforms.color.value = new Color(colors.cyan)
    this.smallShaderMaterialCyan.uniforms.edgecolor.value = new Color(colors.cyan)

    this.smallShaderMaterialYellow = this.bigShaderMaterial.clone()
    this.smallShaderMaterialYellow.uniforms.color.value = new Color(colors.yellow)
    this.smallShaderMaterialYellow.uniforms.edgecolor.value = new Color(colors.yellow)

    this.smallMaterials = [
      this.smallShaderMaterialBlue,
      this.smallShaderMaterialMagenta,
      this.smallShaderMaterialCyan,
      this.smallShaderMaterialYellow
    ]
  }

  // Creates the thicc donut in front of all others
  createTHICCDonut() {
    this.baseGeometry = new TorusBufferGeometry(150, 22, 256, 128)
    this.donutTHICC = new Mesh(this.baseGeometry, this.bigShaderMaterial)
    this.add(this.donutTHICC)
  }

  // Creates a donuts grid with a small scale and a random color for each one
  createSmallDonuts(numberOfLines = 5, numberOfColumns = 8) {
    this.backDonuts = new Group()

    this.baseGeometrySmall = new TorusBufferGeometry(30, 7, 256, 128)
    this.donutMesh = new Mesh(this.baseGeometrySmall, this.bigShaderMaterial)

    for (let column = 0; column < numberOfColumns; column++) {
      for (let line = 0; line < numberOfLines; line++) {
        const donutMesh = this.donutMesh.clone()

        donutMesh.position.x = 125 * column - this.viewport.width / 2
        donutMesh.position.y = 125 * line - this.viewport.height / 2
        donutMesh.material = this.smallMaterials[Math.floor(Math.random() * this.smallMaterials.length)]
        donutMesh.scale.set(this.smallScale, this.smallScale, this.smallScale)
        donutMesh.animating = false

        this.backDonuts.add(donutMesh)
      }
    }

    this.backDonuts.position.z = -300

    this.add(this.backDonuts)
  }

  // Rotates a random donut to 180 (even index) or -180 (odd index)
  rotateRandomDonut() {
    let donut
    let index

    do {
      index = Math.floor(Math.random() * this.backDonuts.children.length)

      donut = this.backDonuts.children[index]
    } while (donut.animating)

    donut.animating = true

    const rotation = index % 3 == 0 ? 180 : -180

    TweenMax.to(donut.rotation, 0.9, {
      y: donut.rotation.y + rotation * Math.PI / 180,
      onComplete: () => {
        donut.animating = false
      }
    })
  }

  // Zoom the specified number of donuts
  zoomSomeDonuts(number = 1) {
    for (let i = 0; i < number; i++) {
      const randomIndex = Math.floor(Math.random() * this.backDonuts.children.length)

      TweenMax.fromTo(this.backDonuts.children[randomIndex].scale, 0.5, {
        x: 0.6,
        y: 0.6,
        z: 0.6
      }, {
        x: 1,
        y: 1,
        z: 1
      })
    }

    this.calmDownIfNeeded()
  }

  // Reduces the number of donut rotations one second later
  calmDownIfNeeded() {
    if (this.thresholdRotation != this.thresholdRotationNormal && !this.wait) {
      this.wait = true

      TweenMax.delayedCall(1, () => {
        this.thresholdRotation = this.thresholdRotationNormal
        this.wait = false
      })
    }
  }

  // The THICC donut bumps
  bumpTHICCDonut() {
    TweenMax.set(this.donutTHICC.scale, {
      x: this.bumpedScaleTHICCDonut,
      y: this.bumpedScaleTHICCDonut,
      z: this.bumpedScaleTHICCDonut
    })
  }

  // The THICC donut reset its scale with a transition
  resetTHICCDonut() {
    TweenMax.to(this.donutTHICC.scale, 0.35, { x: 1, y: 1, z: 1 })
  }

  // Does all the stuff when a big kick happens (> 0.61)
  // Increases the number of donut rotations
  // If the THICC donut is not already changing its color, change it randomly
  // Bumps all the donuts at the same time
  bigKick() {
    if (this.thresholdRotation != this.thresholdRotationFullspeed) this.thresholdRotation = this.thresholdRotationFullspeed

    if (!this.donutTHICC.isChangingColor) {
      const color = getRandomColor(this.bigShaderMaterial.uniforms.color.value)

      this.donutTHICC.isChangingColor = true

      TweenMax.to([this.bigShaderMaterial.uniforms.color.value, this.bigShaderMaterial.uniforms.edgecolor.value], 0.25, {
        r: color.r,
        g: color.g,
        b: color.b,
        onComplete: () => {
          this.donutTHICC.isChangingColor = false
        }
      })

      this.bumpAllDonuts()
    }
  }

  // Bumps all the donuts at the same time
  bumpAllDonuts() {
    for (let i = 0; i < this.backDonuts.children.length; i++) {
      TweenMax.fromTo(this.backDonuts.children[i].scale, 0.25, {
        x: 1.3,
        y: 1.3,
        z: 1.3
      }, {
        x: 1,
        y: 1,
        z: 1
      })

      this.backDonuts.children[i].material = this.smallMaterials[Math.floor(Math.random() * this.smallMaterials.length)]
    }
  }

  // Checks if the average sound is calm
  // If so, scale down one random donut at a time to its inital scale
  checkIfHasToCalmTheFuckDown(average) {
    if (average > 8) return

    this.lowFrequencyCounter++

    if (this.lowFrequencyCounter % 4 !== 0) return

    let randomIndex
    let tries = 0

    do {
      tries++
      randomIndex = Math.floor(Math.random() * this.backDonuts.children.length)
    } while (this.backDonuts.children[randomIndex].scale.x == 0.05 && tries <= this.backDonuts.children.length / 2)

    TweenMax.to(this.backDonuts.children[randomIndex].scale, 0.4, {
      x: this.smallScale,
      y: this.smallScale,
      z: this.smallScale
    })
  }

  updateShaderMaterials(light, time) {
    const lightPos = light.position.clone()

    // Update light position to change the white reflexion
    this.bigShaderMaterial.uniforms.lightPosition.value = lightPos
    this.smallShaderMaterialBlue.uniforms.lightPosition.value = lightPos
    this.smallShaderMaterialMagenta.uniforms.lightPosition.value = lightPos
    this.smallShaderMaterialCyan.uniforms.lightPosition.value = lightPos
    this.smallShaderMaterialYellow.uniforms.lightPosition.value = lightPos

    // I don't know if this uniform is used but nevermind
    this.bigShaderMaterial.uniforms.time.value = time
    this.smallShaderMaterialBlue.uniforms.time.value = time
    this.smallShaderMaterialMagenta.uniforms.time.value = time
    this.smallShaderMaterialCyan.uniforms.time.value = time
    this.smallShaderMaterialYellow.uniforms.time.value = time
  }

  // Updates the material with the correct time and light position
  // Rotates the thicc donut constantly
  // Rotates a random donut randomly aproximatively every 500ms
  render(dt, time, light) {
    if (!this.loaded) return

    this.donutTHICC.rotation.y -= 0.6 * dt

    this.updateShaderMaterials(light, time)

    if (Math.floor(time * 1000) % 500 <= this.thresholdRotation * 2) this.rotateRandomDonut()
  }
}
