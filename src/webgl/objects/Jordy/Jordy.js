import {
  Group,
  Color,
  Mesh,
  SpotLight,
  AmbientLight,
  PlaneBufferGeometry,
  MeshPhongMaterial
} from 'three'
import Donuts from './Donuts'
import { TweenMax } from 'gsap'
import { getViewport } from '../../../utils/Screen'
import Sound from '../../../utils/Sound'
import { getRandomColor } from '../../../utils/Colors'

class Jordy extends Group {
  constructor() {
    super()

    this.name = 'jordy'

    this.didKick = false
    this.lowFrequencyCounter = 0
    this.wait = false
    this.viewport = getViewport()

    this.createBackPlane()
    this.createLight()

    this.donuts = new Donuts()

    this.donuts.load(this.light)
      .then(() => {
        this.add(this.donuts)
      })
  }

  // The wonderful background receiving the light
  createBackPlane() {
    const plane = new PlaneBufferGeometry(this.viewport.width, this.viewport.height)
    const material = new MeshPhongMaterial({ color: new Color('white') })
    this.plane = new Mesh(plane, material)
    this.plane.position.z = -400
    this.plane.isChangingColor = false

    this.add(this.plane)
  }

  // Spotlight (moving constantly) and ambient
  createLight() {
    this.light = new SpotLight(0xffffff, 0.25, 1200, Math.PI / 12)
    this.light.position.set(125, 25, 650)

    this.ambientLight = new AmbientLight(0xe0b9b9, 0.2)

    this.add(this.ambientLight)
    this.add(this.light)
  }

  // Checks kicks power from value
  isMiddleKick(value) {
    const minThreshold = 0.50
    const maxThreshold = 0.54

    if (value >= minThreshold && value <= maxThreshold) return true

    return false
  }

  isKick(value) {
    const threshold = 0.54

    if (value > threshold) return true

    return false
  }

  isBigKick(value) {
    const threshold = 0.61

    if (value > threshold) return true

    return false
  }

  isTHICCKick(value) {
    const threshold = 0.635

    if (value > threshold) return true

    return false
  }

  // Changes the plane color randomly
  changePlaneColor() {
    if (this.plane.isChangingColor) return

    this.plane.isChangingColor = true

    const color = getRandomColor(this.plane.material.color, true)

    TweenMax.to(this.plane.material.color, 0.5, {
      r: color.r,
      g: color.g,
      b: color.b,
      onComplete: () => {
        this.plane.isChangingColor = false
      }
    })
  }

  // Speaks by itself
  getAverageFrequency(frequencies) {
    let sum = 0

    for (let i = 0; i < frequencies.length; i++) {
      sum += frequencies[i]
    }

    return sum / frequencies.length
  }

  // Medium kicks generate a number of zoomed donuts
  getNumberOfZooms(value) {
    if (value >= 0.51 && value < 0.52) return 2
    else if (value >= 0.52 && value < 0.53) return 3
    else if (value >= 0.53 && value <= 0.54) return 4

    return 1
  }

  // Moves the light
  // Renders the Donuts
  // Checks the sounds and updates the Donuts accordingly
  update(dt = 0, time = 0, frequencies) {
    if (!this.donuts.loaded) return

    this.light.position.x = Math.sin(time * 0.8) * 300

    this.donuts.render(dt, time, this.light)

    const value = Sound.getAverageAmplitude()
    const averageFrequency = this.getAverageFrequency(frequencies)

    if (this.isMiddleKick(value)) {
      const numberOfZooms = this.getNumberOfZooms(value)

      this.donuts.zoomSomeDonuts(numberOfZooms)
    }

    if (this.isKick(value)) {
      this.didKick = true

      this.donuts.bumpTHICCDonut()

      if (this.isBigKick(value)) {
        this.donuts.bigKick()

        if (this.isTHICCKick(value)) this.changePlaneColor()
      }
    } else if (this.didKick) {
      this.didKick = false

      this.donuts.resetTHICCDonut()
    }

    this.donuts.checkIfHasToCalmTheFuckDown(averageFrequency)
  }
}

export default Jordy
