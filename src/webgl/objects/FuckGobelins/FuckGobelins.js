import { Group, Mesh, Color, SphereBufferGeometry } from 'three';
import Shape from './Shape';
import { gui } from '@/webgl';
import { average } from '@/utils/Array';
import U from './Utils';
import { getViewport } from '@/utils/Screen';
import mapping from './mapping.json';
import Sound from '@/utils/Sound';

class FuckGobelins extends Group {
  constructor() {
    super();

    this.name = 'fuckGobelins';

    this.mapping = mapping.reverse();

    this.setup();
    this.initShapes();
  }

  setup() {
    this.ratio = { x: 9, y: 16 };

    this.density = 3;

    this.vertices = 32;

    this.grid = {
      x: this.ratio.x * this.density,
      y: this.ratio.y * this.density
    };
    this.full = getViewport().height;
    this.offset = 12 / this.density;
    this.w2 = this.full / 2;
    this.shapes = [];

    this.colors = {
      a: ['#3563AD', '#FEC700', '#EA4E82', '#6AC7E6'],
      c: {
        dBlue: '#3563AD',
        yellow: '#FEC700',
        pink: '#EA4E82',
        lBlue: '#6AC7E6'
      }
    };
  }

  initShapes() {
    this.wrapper = new Group();
    this.wrapper.position.x = -getViewport().width / 2 + this.offset / 2;
    this.wrapper.position.y = -getViewport().height / 2;

    this.add(this.wrapper);

    this.state = {
      big: false,
      psy: false,
      blocked: false,
      hacked: false
    };

    const anim = {
      normal: {
        vibrate: {
          psy: { min: -0.2, max: 0.4 },
          normal: { min: 0.1, max: 0.4 }
        }
      },
      big: {
        vibrate: {
          blocked: { min: 0, max: 1.4 },
          normal: { min: 0.2, max: 1.4 }
        }
      }
    };

    this.d = (this.full - (this.grid.x + 1) * this.offset) / this.grid.x;

    this.geometry = new SphereBufferGeometry(
      this.d / 2,
      this.vertices,
      this.vertices
    );

    this.mesh = new Mesh(this.geometry, null);

    let injectedColors;
    let pos = { x: 0, y: 0 };

    for (let line = 1; line <= this.grid.x; line += 1) {
      for (let column = 1; column <= this.grid.y; column += 1) {
        pos.x = column;
        pos.y = line;

        injectedColors = {
          normal:
            Math.random() > 0.5
              ? new Color(this.colors.c.yellow)
              : new Color(this.colors.c.pink),
          big: new Color(this.colors.c.lBlue),
          hacked: new Color(this.colors.c.yellow),
          hackedAlt: new Color(this.colors.c.pink),
          blocked: new Color(this.colors.c.lBlue)
        };

        if (this.mapping[line - 1][column - 1] === 2) {
          injectedColors.blocked = new Color(this.colors.c.dBlue);
        }

        const state = { ...this.state };

        state.blocked = this.mapping[line - 1][column - 1] >= 1;

        if (state.blocked) {
          const shape = new Shape({
            r: this.d / 2,
            pos,
            offset: this.offset,
            w2: this.w2,
            anim,
            state,
            colors: injectedColors,
            mesh: this.mesh.clone()
          });

          this.wrapper.add(shape);
          this.shapes.push(shape);
        }
      }
    }

    new TimelineMax({
      repeatDelay: 1,
      repeat: -1,
      yoyo: true,
      onRepeat: () => {
        this.shapes.forEach(shape => {
          shape.psy = Math.random() < 0.5;
        });
      }
    });
  }

  update(dt = 0, time = 0, frequencies = []) {
    this.isDrop(dt, time, frequencies);
  }

  isDrop(dt, time, frequencies) {
    let value = average(frequencies);

    const blinkThreshold = 0.3;
    const bigThreshHold = 0.55;

    let valor = 0;
    this.shapes.forEach(shape => {
      if (!shape.blocked) {
        if (Sound.getAverageAmplitude() > blinkThreshold) {
          valor = 0.6 + Math.sin(time * 0.05) * 0.2;
          shape.setBlink(valor < Math.random());
        }
      }
    });

    // this.initBass
  }
}

export default FuckGobelins;
