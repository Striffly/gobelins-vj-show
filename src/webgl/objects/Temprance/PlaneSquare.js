import {
  PlaneBufferGeometry,
  Mesh,
  Object3D,
  MeshBasicMaterial,
  Color
} from 'three';
import { average } from '../../../utils/Array';

class PlaneSquare extends Object3D {
  constructor(color) {
    super();

    this.maxPosX = 500;
    this.maxPosY = 500;
    this.maxSize = 40;
    this.minSize = 30;

    this.color = color;
    this.lastTimePic = 0;
    this.wave = 0;

    // Set random size
    let size = this.getRandomSize();
    let geometry = new PlaneBufferGeometry(size, size / 2);

    // Default
    this.currentColor = color[this.getRandomIndexColor()];
    this.material = new MeshBasicMaterial({ color: this.currentColor });
    this.mesh = new Mesh(geometry, this.material);

    // Set random position
    let position = this.getRandomPosition();
    this.mesh.position.set(position.x, position.y, 0.1);

    // Set random scalingValue
    this.scaleValue = Math.random() > 0.5 ? 2.5 : 1;

    // ADD border black
    let childSizeDifference = 2;
    let geometryChild = new PlaneBufferGeometry(
      size - childSizeDifference,
      size / 2 - childSizeDifference
    );
    let materialChild = new MeshBasicMaterial({ color: '#000' });
    let childMesh = new Mesh(geometryChild, materialChild);

    this.mesh.add(childMesh);

    if (Math.random() > 0.5) {
      let geometryChild = new PlaneBufferGeometry(
        size - childSizeDifference * 2,
        size / 2 - childSizeDifference * 2
      );
      let materialChild = new MeshBasicMaterial({ color: this.currentColor });
      this.childMesh = new Mesh(geometryChild, materialChild);

      this.mesh.add(this.childMesh);
    }

    this.add(this.mesh);
  }

  getRandomSize() {
    return Math.random() * (this.maxSize - this.minSize) + this.minSize;
  }

  getRandomPosition() {
    return {
      x: Math.random() * this.maxPosX * 2 - this.maxPosX,
      y: Math.random() * this.maxPosY * 2 - this.maxPosY
    };
  }

  getRandomIndexColor() {
    return Math.round(Math.random() * (this.color.length - 1));
  }

  changeColor() {
    this.currentColor = new Color(this.color[this.getRandomIndexColor()]);

    // MESH COLOR
    this.mesh.material.color = this.currentColor;
    if (this.childMesh) {
      this.childMesh.material.color = this.currentColor;
    }
  }

  update(dt = 0, time = 0, frequencies) {
    this.wave = frequencies[25] / 255;

    // SCALE
    let reduceur = Math.random() > 0.5 ? 0.1 : -0.1;
    let scale = 1 + (this.scaleValue + reduceur) * this.wave;
    this.mesh.scale.set(scale, scale, 1);

    const value = average(frequencies);

    let min = 100;
    let max = 120;
    const threshold = 0.3;

    if (value < min) {
      min = value;
    }

    if (value > max) {
      max = value;
    }

    const range = max - min || 1;
    const norm = (value - min) / range;

    if (norm > threshold) {
      if (time - this.lastTimePic > 0.1) {
        // COLOR
        this.changeColor();

        // POSITION
        let position = this.getRandomPosition();
        this.mesh.position.set(position.x, position.y, 0.1);

        // ROTATION
        this.mesh.rotation.z += 90;

        this.lastTimePic = time;
      }
    }
  }
}

export default PlaneSquare;
