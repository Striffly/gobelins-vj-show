import { Group } from 'three';
import PlaneBG from './PlaneBG';
import PlaneSquare from './PlaneSquare';
import { gui } from '@/webgl';

class Wrapper extends Group {
  constructor() {
    super();

    this.name = 'temprance';

    let colors = ['#3563AD', '#FEC700', '#EA4E82', '#6AC7E6'];

    let planes = [];

    for (let i = 0; i <= 120; i++) {
      planes.push(new PlaneSquare(colors));
    }

    for (let plane of planes) {
      this.add(plane);
    }
  }
}

export default Wrapper;
