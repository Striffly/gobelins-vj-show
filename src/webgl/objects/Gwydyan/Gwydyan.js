import {
  Color,
  Mesh,
  MeshBasicMaterial,
  Object3D,
  PlaneBufferGeometry
} from 'three';
import { TweenMax } from 'gsap';
import { getViewport } from '../../../utils/Screen';
import Sound from '../../../utils/Sound';

class Gwydyan extends Object3D {
  constructor() {
    super();

    this.init = false;
    this.name = 'gwydyan';
    this.oldTime = 0;
    this.colors = ['#3563AD', '#FEC700', '#EA4E82', '#6AC7E6'];

    this.createGrid();
  }

  anime(mesh) {
    TweenMax.to(mesh.scale, 0.1, {
      x: 1.5,
      y: 1.5,
      z: 1.5,
      ease: Quart.easeOut,
      onComplete: () => {
        TweenMax.to(mesh.scale, 0.1, {
          x: 1,
          y: 1,
          z: 1,
          ease: Quart.easeOut,
          onComplete: () => {
            mesh.material.color = new Color('#000');
          }
        });
      }
    });
    const randomIndexColor = Math.round(
      Math.random() * (this.colors.length - 1)
    );
    mesh.material.color = new Color(this.colors[randomIndexColor]);
  }

  createGrid() {
    const viewport = getViewport();

    const caseWidth = 25;
    const caseHeight = 25;

    const nbColumn = viewport.width / caseWidth;
    const nbLines = viewport.height / caseHeight;

    this.stepValue = nbLines;

    this.whiteSquares = [];
    let lines = [];

    for (let y = 0; y <= nbLines - 1; y++) {
      for (let x = 0; x <= nbColumn - 1; x++) {
        if (x % 2 == 0 && y % 2 == 0) {
          const posX =
            -viewport.width / 2 + caseWidth * 0.8 + (x % nbColumn) * caseWidth;
          const posY = -viewport.height / 2 + caseHeight * 1 + y * caseHeight;

          let geometry = new PlaneBufferGeometry(caseWidth, caseHeight);
          let material = new MeshBasicMaterial({ color: '#000' });
          let mesh = new Mesh(geometry, material);
          mesh.position.set(posX, posY, 0.1);
          this.add(mesh);

          // if ( Math.random() < 0.15) {
          //     let geometryChild1 = new PlaneGeometry( 4 , caseHeight - 4 );
          //     let geometryChild2 = new PlaneGeometry( caseWidth - 4 , 4 );
          //
          //     let materialChild = new MeshBasicMaterial({color: new Color('#000')});
          //
          //     let childMesh1 = new Mesh(geometryChild1, materialChild);
          //     let childMesh2 = new Mesh(geometryChild2, materialChild);
          //
          //     mesh.add(childMesh1,childMesh2);
          // }  else if (Math.random() < 0.15){
          //
          //     let geometryChild3 = new PlaneGeometry(caseWidth - 5 , caseHeight - 5 );
          //     let materialChild3 = new MeshBasicMaterial({color: new Color('#000')});
          //     let childMesh3 = new Mesh(geometryChild3, materialChild3);
          //
          //     mesh.add(childMesh3);
          // }

          lines.push(mesh);
        }
      }
      this.whiteSquares.push(lines);
      lines = [];
    }

    this.init = true;
  }

  update(dt = 0, time = 0, frequencies) {
    if (!this.init) {
      return;
    }

    let frequency = Sound.getAverageAmplitude();
    let arrayStep = Math.round(this.stepValue * frequency);

    if (time - this.oldTime <= 0.2 && arrayStep == 10) {
      return;
    }
    this.oldTime = time;

    if (arrayStep != 0 && frequency != 0.5) {
      for (let mesh of this.whiteSquares[arrayStep]) {
        this.anime(mesh);
      }
    }
  }
}

export default Gwydyan;
