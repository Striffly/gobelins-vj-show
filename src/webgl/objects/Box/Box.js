import {
  Object3D,
  BoxBufferGeometry,
  ShaderMaterial,
  Mesh,
  Color
} from 'three';
import { canvas, webgl, gui } from '@/webgl';
import { vertexShader, fragmentShader } from '@/webgl/shaders/box/box.shader';
import { average } from '../../../utils/Array';

class Box extends Object3D {
  constructor({ initialScale }) {
    super();

    this.initialScale = initialScale;

    const uniforms = {
      time: { value: 1.0 },
      colorA: { value: new Color('#ea4e82') },
      colorB: { value: new Color('#fec700') }
    };

    const geometry = new BoxBufferGeometry(50, 50, 50);
    this.material = new ShaderMaterial({
      uniforms: uniforms,
      vertexShader,
      fragmentShader
    });

    const mesh = new Mesh(geometry, this.material);

    this.add(mesh);

    this.scale.set(initialScale, initialScale, initialScale);

    this.settings = {
      rotationSpeed: 0.5,
      pulseSpeed: 2.0,
      colorA: this.material.uniforms.colorA.value.getStyle(),
      colorB: this.material.uniforms.colorB.value.getStyle()
    };

    const update = () => {
      this.material.uniforms.colorA.value.setStyle(this.settings.colorA);
      this.material.uniforms.colorB.value.setStyle(this.settings.colorB);
    };
  }

  update(dt = 0, time = 0, frequencies) {
    const start = 10;
    const range = 20;

    let trimedFrequencies = [];

    for (let i = start; i < start + range; i++) {
      trimedFrequencies.push(frequencies[i]);
    }

    trimedFrequencies = average(trimedFrequencies);

    this.material.uniforms.time.value = time * this.settings.pulseSpeed;

    // const scaleAmplitude = trimedFrequencies * 0.5 + 100;
    const scaleAmplitude = trimedFrequencies * 0.25 + 0.75;
    const scale = dt * scaleAmplitude * this.initialScale;

    this.scale.set(scale, scale, scale);
    this.rotation.x += dt * this.settings.rotationSpeed;
    this.rotation.y += dt * this.settings.rotationSpeed;
  }
}

export default Box;
