import { Object3D, ShaderMaterial } from 'three';
import { vertexShader, fragmentShader } from './glow/glow.shader';
import U from './Utils';

class Shape extends Object3D {
  constructor(props) {
    super();
    this.props = props;

    this.sinOffset = Math.random();

    this.big = this.props.state.big;
    this.psy = this.props.state.psy;
    this.blocked = this.props.state.blocked;
    this.hacked = this.props.state.hacked;
    this.mesh = this.props.mesh;

    this.mesh.rotation.x = 1.5708;

    this.position.x =
      (this.props.pos.x - 1) * this.props.r * 2 +
      this.props.r +
      this.props.offset * this.props.pos.x;
    this.position.y =
      (this.props.pos.y - 1) * this.props.r * 2 +
      this.props.r +
      this.props.offset * this.props.pos.y;

    this.uniforms = {
      color: {
        value: this.blocked
          ? this.props.colors.blocked
          : this.props.colors.normal
      },
      start: { value: this.blocked ? 0.2 : 0.4 },
      end: { value: 1 },
      alpha: { value: 1 }
    };

    this.material = new ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader,
      fragmentShader
    });

    this.mesh.material = this.material;

    this.add(this.mesh);
  }

  update(dt = 0, time = 0, frequencies = []) {
    if (!this.blocked) {
      this.render(dt, time, frequencies);
    } else {
      this.renderBlocked(dt, time, frequencies);
    }
  }

  render(dt, time, frequencies) {
    this.hack(dt);
    this.vibrate(time, frequencies);
  }

  renderBlocked(dt, time, frequencies) {
    this.blockedVibrate(time, frequencies);
  }

  hack(dt) {
    if (!this.hacked) {
      let valor = Math.random() * dt * 100;
      if (valor < 0.01) {
        this.hacked = true;
        setTimeout(() => {
          this.hacked = false;
        }, randomInt(500, 2000));
      }
    }
  }

  blockedVibrate(time, frequencies) {
    let mapped = U.map(
      frequencies[25] / 255,
      0,
      1,
      this.props.anim.big.vibrate.blocked.min,
      this.props.anim.big.vibrate.blocked.max
    );
    if (frequencies[25] === 0) {
      mapped = 1;
    }

    this.scale.set(mapped, mapped, mapped);
  }

  vibrate(time, frequencies) {
    let scale = this.psy ? Math.sin(time * 300) : Math.sin(time * 100);
    let mapped = 1;

    if (this.big) {
      // if(this.psy) {
      //   mapped = U.map(scale, -1, 1, this.props.anim.big.vibrate.psy.min, this.props.anim.big.vibrate.psy.max);
      // } else {
      //   mapped = U.map(scale, -1, 1, this.props.anim.big.vibrate.normal.min, this.props.anim.big.vibrate.normal.max);
      // }
      mapped = U.map(
        frequencies[25] / 255,
        0,
        1,
        this.props.anim.big.vibrate.normal.min,
        this.props.anim.big.vibrate.normal.max
      );
    } else {
      if (this.psy) {
        mapped = U.map(
          scale,
          -1,
          1,
          this.props.anim.normal.vibrate.psy.min,
          this.props.anim.normal.vibrate.psy.max
        );
      } else {
        mapped = U.map(
          scale,
          -1,
          1,
          this.props.anim.normal.vibrate.normal.min,
          this.props.anim.normal.vibrate.normal.max
        );
      }
    }
    this.scale.set(mapped, mapped, mapped);
  }

  setBlink(blink) {
    if (blink) {
      this.mesh.material.wireframe = true;
      this.mesh.material.wireframeLinewidth = Math.random() * 3;
    } else {
      this.mesh.material.wireframe = false;
    }
  }

  setBig(big = false) {
    if (big) {
      this.big = true;
      if (this.hacked) {
        this.uniforms.color = {
          value:
            Math.random() < 0.3
              ? this.props.colors.hackedAlt
              : this.props.colors.hacked
        };
      } else {
        this.uniforms.color = { value: this.props.colors.big };
      }
    } else {
      this.big = false;
      this.uniforms.color = { value: this.props.colors.normal };
    }
  }

  defineIsPsy() {
    this.psy = false;
  }
}

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

/* return %tage = of value in the range [ min - max ] / 100 */
function norm(value, min, max) {
  return (value - min) / (max - min);
}

/* return value = of percentage in the range [ min - max ] */
function lerp(norma, min, max) {
  return (max - min) * norma + min;
}

export default Shape;
