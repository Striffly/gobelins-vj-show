import { Group, Mesh, AmbientLight, Color, SphereBufferGeometry } from 'three';
import { TimelineMax } from 'gsap/all';
import Shape from './Shape';
import { gui } from '@/webgl';
import { average } from '@/utils/Array';
import U from './Utils';
import { getViewport } from '@/utils/Screen';
import Sound from '@/utils/Sound';

class FuckGosu extends Group {
  constructor() {
    super();

    this.name = 'fuckGosu';

    this.setup();
    this.initShapes();
  }

  setup() {
    this.ratio = { x: 9, y: 16 };

    this.density = 1;

    this.canSwitch = true;

    this.vertices = [8, 16, 32];

    this.current = randomInt(0, 2);

    this.grid = {
      x: this.ratio.x * this.density,
      y: this.ratio.y * this.density
    };
    this.full = getViewport().height;
    this.offset = 12 / this.density;
    this.w2 = this.full / 2;
    this.shapes = [];

    this.colors = {
      a: ['#3563AD', '#FEC700', '#EA4E82', '#6AC7E6'],
      c: {
        dBlue: '#3563AD',
        yellow: '#FEC700',
        pink: '#EA4E82',
        lBlue: '#6AC7E6'
      }
    };
  }

  initShapes() {
    this.wrappers = [];
    this.shapesCollection = [];

    this.state = {
      big: false,
      psy: false,
      blocked: false,
      hacked: false
    };

    const anim = {
      normal: {
        vibrate: {
          psy: { min: -0.2, max: 0.4 },
          normal: { min: 0.1, max: 0.4 }
        }
      },
      big: {
        vibrate: {
          blocked: { min: 0, max: 1.4 },
          normal: { min: 0.2, max: 1.4 }
        }
      }
    };

    this.d = (this.full - (this.grid.x + 1) * this.offset) / this.grid.x;

    this.vertices.forEach((vertice, i) => {
      const shapes = [];
      const wrapper = new Group();
      wrapper.position.x = -getViewport().width / 2 + this.offset / 2;
      wrapper.position.y = -getViewport().height / 2;

      const geometry = new SphereBufferGeometry(this.d / 2, vertice, vertice);
      const mesh = new Mesh(geometry, null);

      let injectedColors;
      let pos = { x: 0, y: 0 };

      for (let line = 1; line <= this.grid.x; line += 1) {
        for (let column = 1; column <= this.grid.y; column += 1) {
          pos.x = column;
          pos.y = line;

          injectedColors = {
            normal: new Color(this.colors.c.dBlue),
            big: new Color(this.colors.c.lBlue),
            hacked: new Color(this.colors.c.yellow),
            hackedAlt: new Color(this.colors.c.pink),
            blocked: new Color(this.colors.c.dBlue)
          };

          const state = { ...this.state };

          const shape = new Shape({
            r: this.d / 2,
            pos,
            offset: this.offset,
            w2: this.w2,
            anim,
            state,
            colors: injectedColors,
            mesh: mesh.clone()
          });

          wrapper.add(shape);
          shapes.push(shape);
        }
      }

      this.shapesCollection.push(shapes);
      this.add(wrapper);
      this.wrappers.push(wrapper);
    });

    this.wrappers.forEach(wrapper => {
      wrapper.visible = false;
    });
    this.wrappers[this.current].visible = true;

    new TimelineMax({
      repeatDelay: 1,
      repeat: -1,
      yoyo: true,
      onRepeat: () => {
        this.shapesCollection.forEach(shapes => {
          shapes.forEach(shape => {
            shape.psy = Math.random() < 0.5;
          });
        });
      }
    });
  }

  update(dt = 0, time = 0, frequencies = []) {
    // this.rotation.z += 2;
    this.isDrop(dt, time, frequencies);
  }

  isDrop(dt, time, frequencies) {
    let value = average(frequencies);

    const blinkThreshold = 0.3;
    const bigThreshHold = 0.55;
    const switchThreshHold = bigThreshHold;

    let valor = 0;
    this.shapesCollection.forEach(shapes => {
      shapes.forEach(shape => {
        if (!shape.blocked) {
          if (Sound.getAverageAmplitude() > bigThreshHold) {
            valor = 0.6 + Math.sin(time * 0.05) * 0.2;
            shape.setBig(valor < Math.random());
          }

          if (Sound.getAverageAmplitude() > blinkThreshold) {
            valor = 0.6 + Math.sin(time * 0.05) * 0.2;
            shape.setBlink(valor < Math.random());
          }

          if (Sound.getAverageAmplitude() > switchThreshHold) {
            this.nextSwitch();
          }
        }
      });
    });
  }

  nextSwitch() {
    if (this.canSwitch) {
      this.canSwitch = false;

      this.current += 1;
      if (this.current > this.wrappers.length - 1) {
        this.current = 0;
      }

      this.wrappers.forEach(wrapper => {
        wrapper.visible = false;
      });
      this.wrappers[this.current].visible = true;

      setTimeout(() => {
        this.canSwitch = true;
      }, 500);
    }
  }
}

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export default FuckGosu;
