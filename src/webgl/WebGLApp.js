import * as THREE from 'three';
import { WebGLRenderer, OrthographicCamera, Scene, AxesHelper } from 'three';
import rightNow from 'right-now';
const OrbitControls = require('three-orbit-controls')(THREE);
import TweenMax from 'gsap/TweenMax';
import Sound from '../utils/Sound';
import { average } from '../utils/Array';

class WebGLApp {
  constructor(opts = {}) {
    this.debug = opts.debug || false;

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: false,
      failIfMajorPerformanceCaveat: true,
      ...opts
    });

    this.hasSwitched = false;
    this.switchDelay = 0.1;
    this.currentScene = null;

    this.canvas = this.renderer.domElement;

    const background = opts.background || '#000';
    this.renderer.setClearColor(background, 1);

    // clamp pixel ratio for performance
    this.maxPixelRatio = 1;

    // clamp delta to stepping anything too far forward
    this.maxDeltaTime = 1 / 30;

    // setup a basic camera
    this.frustumSize = 500;

    var aspect = window.innerWidth / window.innerHeight;
    const near = 0.001;
    const far = 2000;
    this.camera = new OrthographicCamera(
      (this.frustumSize * aspect) / -2,
      (this.frustumSize * aspect) / 2,
      this.frustumSize / 2,
      this.frustumSize / -2,
      near,
      far
    );
    this.camera.position.z = 400;

    // set up a simple orbit controller
    if (this.debug) {
      this.controls = new OrbitControls(this.camera, this.canvas);
      this.controls.update();
    }

    this.time = 0;
    this.isRunning = false;
    this.lastTime = rightNow();
    this.rafID = null;

    this.scene = new Scene();

    // setup help for debugging
    if (this.debug) {
      this.scene.add(new AxesHelper(200));
    }

    // handle resize events
    window.addEventListener('resize', () => this.resize());
    window.addEventListener('orientationchange', () => this.resize());

    // force an initial resize event
    this.resize();
  }

  resize(width = window.innerWidth, height = window.innerHeight) {
    const aspect = window.innerWidth / window.innerHeight;
    this.camera.left = (-this.frustumSize * aspect) / 2;
    this.camera.right = (this.frustumSize * aspect) / 2;
    this.camera.top = this.frustumSize / 2;
    this.camera.bottom = -this.frustumSize / 2;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    // draw a frame to ensure the new size has been registered visually
    this.draw();
    return this;
  }

  update(dt = 0, time = 0) {
    if (this.debug) {
      this.controls.update();
    }

    // send sound frequencies to all scene
    const frequencies = Sound.getFrequencies();

    // recursively tell all child objects to update
    this.scene.traverse(obj => {
      if (typeof obj.update === 'function') {
        obj.update(dt, time, frequencies);
      }
    });

    return this;
  }

  trimFrequencies(frequencies) {
    const start = 0;
    const range = 40;

    let trimedFrequencies = [];

    for (let i = start; i < start + range; i++) {
      trimedFrequencies.push(frequencies[i]);
    }

    return trimedFrequencies;
  }

  verifyFrequencies(frequencies) {
    const value = Sound.getAverageAmplitude();

    let min = 0;
    let max = 1;
    const threshold = 0.7;

    if (value > threshold) {
      if (!this.hasSwitched) {
        this.toggleScene(this.getRandomScene());

        this.hasSwitched = true;
        TweenMax.delayedCall(0.1, () => {
          this.hasSwitched = false;
        });
      }
    }
  }

  switchScene() {
    setTimeout(() => {
      this.toggleScene(this.getRandomScene());
      this.switchScene()
    }, 10000);
  }

  draw() {
    this.renderer.render(this.scene, this.camera);
    return this;
  }

  start() {
    if (this.rafID !== null) {
      return;
    }

    // choose a random scene between all available
    const scene = this.getRandomScene();

    this.toggleScene(scene);

    this.rafID = window.requestAnimationFrame(this.animate);
    this.isRunning = true;
    this.switchScene();
    return this;
  }

  getRandomScene() {
    const availableScenes = this.scenes.filter(scene => {
      return scene != this.currentScene;
    });

    const sceneToShow =
      availableScenes[Math.floor(Math.random() * availableScenes.length)];

    return sceneToShow;
  }

  toggleScene(name) {
    console.log('WebGLApp: toggleScene ' + name);

    // recursively tell all scenes to hide
    this.scene.traverse(obj => {
      if (this.scenes.includes(obj.name)) {
        obj.visible = false;
      }
    });

    // show good scene
    this.scene.traverse(obj => {
      if (obj.name === name) {
        obj.visible = true;
      }
    });

    this.currentScene = name;
  }

  stop() {
    if (this.rafID === null) {
      return;
    }

    window.cancelAnimationFrame(this.rafID);
    this.rafID = null;
    this.isRunning = false;
    return this;
  }

  animate = () => {
    if (!this.isRunning) {
      return;
    }

    window.requestAnimationFrame(this.animate);

    const now = rightNow();
    const dt = Math.min(this.maxDeltaTime, (now - this.lastTime) / 1000);
    this.time += dt;
    this.lastTime = now;
    this.update(dt, this.time);
    this.draw();
  };
}

export default WebGLApp;
