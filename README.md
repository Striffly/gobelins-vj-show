# VJ-Show

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Contribute

**To create your own objects**

Run :
```
npm run object gwydyan
```

Then add it in `Home.vue` like this :
```
this.webgl.scene.add(new Gwydyan());
```

## Todo
- 'About" page with contributors (ScrollMagic on right side ?)
- Select scene
- Use personal music
- Update "Contribute" section
- Objects loaders + tutorials
- True random